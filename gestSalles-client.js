/*
 * projet:conferencier
 * fichier:gestSalles-client.js 
 * auteur: pascal TOLEDO
 * date de creation: 2013.12.12
 * date de modification: 2014.05.03
 * version: 1.0
 *
 * description:code js pour le fichier gestSalles-client.html
 * dependance:
 * - socket.io
 * ce fichier sert a la fois pour le maitre et le client
 */

// var MODE="MAITRE"|"CLIENT" // var heriter du parent.html
var serveurDN='192.168.0.253';
var serveurDN='localhost';
//var serveurDN='mediatheques.legral.fr';
var PORT=11111;


// -- conferences -- //

// --- contenu des conferences --- //
var gestConferences= new TgestConferences();

// --- pilotage des conferences libres (1 seule conference  a la fois) --- //
var confNo=0;var pageNo=0;var pageTitre=''; //page en cours de visionnement confsShowPlan()

// --- conferenceS pilotéS par un (ou plusieurs) maitre(s) de conference (plusieurs conferences sont gérés simultannement) --- //
var gestConfsRun=new TgestConferencesRun();

function connexion()
	{
	var url = 'http://'+serveurDN+':'+PORT;
	consoleAdd('connexion a '+url+'...');
	socket=io.connect(url);

	// on demande la liste des salles
	//consoleAdd('demande de la liste des salles');
	//socket.emit('getData',{'dataId':'salleList'});

        // on demande la liste des conferences
	//consoleAdd('demande de la liste des conferences');
	//socket.emit('getData',{"dataId":"confList"});

	
        socket.on('connectOk',function(data){consoleAdd('conexion etabli:'+socket.socket.open+' host:'+socket.socket.host+' port:'+socket.socket.port+' msg: '+data);});


	// -- COMMUNICATIONS GENERALES -- //
	// --- On affiche une boîte de dialogue quand le serveur nous envoie un "message" --- //
	socket.on('alert', function(message){alert('Le serveur a un message pour vous : ' + message);});

        socket.on('console',function(data){consoleAdd(data);});


	// -- COMMUNICATIONS DE DONNEES -- //

/*
        // -- reception d'un fichier -- //
        socket.on('sendFichier',function(fichier)
                {
                if(!typeOf(fichier)=='object')return null;
                if(!data.fichierId)return null;
 		consoleAdd('reception du fichier: '+fichier.fichierId);
 
		// -- choix du fichier -- //
                switch(fichier.fichierId){
                        case'':break;
			}

		// -- chargemet du fichier -- //
		//fs.

		// --  envoie du fichier-- //
		socket.emit('file',{});
                });
*/

        // -- reception d'une mise a jours de donnees -- //
        // data.id:id des donnees
        // data.content:contenue des donnees: string,int,ou object
        socket.on('setData',function(data){
                if(!typeof(data)=='object')return null;
                if(!data.dataId)return null;
                consoleAdd('reception d\'une mise a jours de donnee: '+data.datId);
                var ar=(data.ar===1)?1:0;
                switch(data.dataId){
                        case'confRun':
                                var fichierID='confRun';
                                consoleAdd('<b>reception de confRun<b>'+data.content);
				break;
                        case'pseudo':
				consoleAdd('<b>Votre pseudo est :</b>'+data.content);
				GLOBALS['pseudo']=data.content;
                                //if(ar)socket.emit('ar','setPseudo');//envoie d'un Accuse reception
				document.getElementById('pseudo').value=data.content;
                                break;

			// --- affichage de la liste des salles --- //
                        case'salleList':
                                consoleAdd('<b>reception de la liste des salles </b>');
                                gestSalles.fullFromRAW(data.content,data.reset);
                                sallesShowListe();	//afficher la nouvelle liste
				//if(ar)socket.emit('ar','setPseudo');//envoie d'un Accuse reception
                                break;

                       // --- affichage de la liste des salles --- //
                        case'confList':
                                consoleAdd('<b>reception de la liste des conferences </b>');
				gestConferences.fullFromRAW(data.content,data.reset);
                                confsShowListe();	//afficher la nouvelle liste
                                //if(ar)socket.emit('ar','setPseudo');//envoie d'un Accuse reception
                                break;

			// --- reception d'un update du tableau des confsRun envoyes par le maitre --- //
//			 case'setConfsRun':
				
//				break;
			
			}
		// -- fonctions resevés AUX CLIENTS -- //

		if(MODE=='client')switch(data.dataId){

			// --- reception d'une mise a jours des conf confs en cours --- //
			case'setconfsRun':
                                t="reception d'une mise a jours des conf confs en cours "+data.content;consoleAdd(t);
				var json=null;
				// ---- verification compatibilité JSON de  data.content ---- //
                                var err=null;
                                try{
                                        json=JSON.parse(data.content);
                                        }
                                catch(e){err=e;}
				 if(json!=null){

					// ---- mise a jours de gestConfsRun.confs avec json ---- //
					gestConfsRun.confs=json;
					// ---- mise a jours de confFrameCSS ---- //
					var pageNu=gestConfsRun.confs.pages[confNo];
					var url=gestConferences.confs[confNo].pages[pageNu].url;
					confFrameCSS.src=url;
					break;
					}
				else{consoleAdd("erreur dans le parsage JSON: "+ data.content);}

                        }

                });



	// -- INTERFACE: EVENEMENTS -- //
 
	// --- pseudo --- //
	document.getElementById('WhoIAm').addEventListener('click',WhoIAm,false);
	function WhoIAm()
                {
                consoleAdd("demande d'envoie du pseudo");
		socket.emit('getData',{dataId:'pseudo'});
                };

	document.getElementById('changePseudo').addEventListener('click',pseudoMaj,false);
	function pseudoMaj()
		{
		consoleAdd('changement de pseudo');
                GLOBALS['pseudo'] =document.getElementById('pseudo').value;
		socket.emit('setData', {'dataId':'pseudo','content':GLOBALS['pseudo']});
		}

	// --- refresh data --- //
        // ---- salles---- //
	document.getElementById('salleListRefresh').addEventListener('click',salleListRefresh,false);
        function salleListRefresh()
                {
                consoleAdd("envoie d'une demande de la liste des salles");
                socket.emit('getData',{"dataId":"salleList"});
                };

        // ---- conferences ---- //
        document.getElementById('getConfListe').addEventListener('click',confsListRefresh,false);
        function confsListRefresh()
                {
                consoleAdd("envoie d'une demande de la liste des conferences");
                socket.emit('getData',{"dataId":"confList","reset":1});
                };

	// ---- demande un mise a jours des donnees du serveur a partir des fichiers de donnéees ---- //
	document.getElementById('getConfUpdate').addEventListener('click',confsListUpdate,false);
	function confsListUpdate()
		{
		consoleAdd("envoie d'une demande de mise a jours des donnees du serveur a partir des fichiers de donnéees.");
                socket.emit('getConfUpdate',{});
                };


        // ---- conferences detail ---- //
        document.getElementById('getConfDetail').addEventListener('click',confsShowPlan,false);

	// -- EXECUTION -- //

	// --- On demande le pseudo au visiteur... --- //
	consoleAdd('pseudo requis.');
	GLOBALS['pseudo']='Ane-O-Nyme';
	//GLOBALS['pseudo'] = prompt('Quel est votre pseudo ?');
	// Et on l'envoie
	consoleAdd('<b>envoie du pseudo:</b> '+ GLOBALS['pseudo']);
	socket.emit('setData', {'dataId':'pseudo','content':GLOBALS['pseudo']});
	document.getElementById('pseudo').value=GLOBALS['pseudo'];


	// -- fonctions resevés AUX MAITRES DE CONFERENCES -- //

        }       // -- function client_events(){

        // --- envoie de la modif de pageNo dans une confNo a tous les clients (broadcast) )(et au serveur?) --- //

	/* -- 
	function onChangePage(data)
	data:obejct
	data.selectIDJS= this (id="planSelect")
	*/
	
	function onChangePage(data)
	        {
	        if(typeof(data)!='object')return undefined;
	        if(!data.selectIDJS)return undefined;
	        var IDJS=data.selectIDJS;
		pageNo=IDJS.selectedIndex;
	        pageTitre=IDJS.options[pageNo].text;
		confFrameCSS.src=IDJS.options[pageNo].value;
	
	        if(MODE=='maitre'){
	                if(gestConfsRun.setPages(confNo,pageNo)!=undefined){
				consoleAdd("Envoi d'un UPDATE des confsRun AU SERVEUR!");
				socket.emit('setData',{"dataId":"setConfsRun","content":JSON.stringify(gestConfsRun.confs)});
	
	                        }
	
	                }
	
        	}

//	}	// -- function connexion(){




function sallesShowListe()
	{
	var datas=gestSalles.datas;	
	var out='<table><caption>Liste des Salles de conferences</caption>';
	out+="<thead><tr><th>No</th><th>nom</th><th>description</th><th>confNo en cours</th></thead><tbody>";
	for(var i in datas)out+='<tr><td>'+i+'</a></td><td>'+datas[i].nom+'</td><td>'+datas[i].description+'</td><td>'+datas[i].confRun+'</td></tr>';
	out+="</tbody></table>"
	document.getElementById('sallesListe').innerHTML=out;
	}

function confsShowListe()
        {
        var confs=gestConferences.confs;
        var out='<table><caption>liste des conferences</caption>';
        out+=             '<thead><tr> <th>No</th>    <th>éditer</th> <th>plan</th>  <th>voir</th> <th>nom</th>  <th>titre</th> <th>description</th> </tr></thead><tbody>\n';
	for(var confNu in confs) out+='<tr> <td><a style="cursor:pointer;"</td> <td></td>       <td><a  title="afficher le detail de la conférence" onclick="confNo='+confNu+';confsShowPlan();">plan</a></td>      <td></td>     <td>'+confs[confNu].nom+'</td>     <td>'+confs[confNu].titre+'</td> <td>'+confs[confNu].description+'</td></tr>\n';
        out+='</tbody></table>';
        document.getElementById('confs').innerHTML=out;
        }

function confsShowPlan(confNu)
        {
	confNu=isNaN(confNu)?confNo:confNu;
	var out="";
	var conf=gestConferences.confs[confNu];
	if(conf){
		var pages=conf.pages;
		if(pages.length==0){
			consoleAdd("pas de page dsponible pour la conf: "+conf.nom+'('+confNu+')');
			return null;
			}
//	        out='<table><caption>plan de la conference: '+conf.nom+'('+confNo+')</caption>';
//	        out+='<thead><tr><th>No</th><th>titre</th></tr></thead><tbody>';
		out+="<div class=''>"+conf.nom+"</div>";
//                out+='<select id="planSelect"  onchange="pageTitre=this.options[this.selectedIndex].text;confFrameCSS.src=this.options[this.selectedIndex].value;">';
		out+='<select id="planSelect"  onchange=\'onChangePage({"selectIDJS":this})\'>';
		out+='<option>choisissez une page... </option>';
		for(var pageNu in pages){
			if(!pageNu)continue;
			if(pages[pageNu].titre[0]=='!')continue;
			url=pages[pageNu].url;	
			var css='optionPageNoSelect';
			var selected='';
			if(pageTitre==pages[pageNu].titre){css='optionPageSelect';selected=' selected="selected" ';}
//			var css=(pageTitre==pages[pageNu].titre)?'optionPageSelect':'optionPageNoSelect'; 
			var disabled=' ';
			if(url=="---")
				{
				disabled=' disabled="disabled"  ';
				css='optionPagePartieTitre';
				}
			out+='<option class="'+css+'"'+selected+disabled+' value="'+url+'" >'+pages[pageNu].titre+'</option>';
			}
		 out+="</select>";
//	        out+='</tbody></table>';
		out+="<script>confFrameCSS.src=conf[0].url</script>";
		}
	else{out="Pas de conférence séléctionnée";}	


        document.getElementById('confs').innerHTML=out;
        }

/* -- 
function onChangePage(data)
data:obejct
data.selectIDJS= this (id="planSelect")

*/
/*
function onChangePage(data)
	{
	if(typeof(data)!='object')return undefined;
	if(!data.selectIDJS)return undefined;
	var IDJS=data.selectIDJS;

	pageTitre=IDJS.options[IDJS.selectedIndex].text;
	pageNo=confFrameCSS.src=IDJS.options[DJS.selectedIndex].value;

	if(MODE=='maitre'){
		if(gestConfsRun.setPages(confNo,pageNo)!=undefined){
			
			}
			
		}

	}

*/
